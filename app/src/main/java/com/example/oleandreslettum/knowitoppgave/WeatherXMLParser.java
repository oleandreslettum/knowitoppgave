package com.example.oleandreslettum.knowitoppgave;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import org.w3c.dom.Document;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

/**
 * Class used to parse XML
 *
 * Created by oleandreslettum on 06.01.2017.
 */

class WeatherXMLParser {

    private XPath xpath = null;
    private String dato;
    private Bitmap image;
    private String symbolId;
    private String temperature;

    /**
     * Class constructor that creates a new xpath-object and constructs a String in the format of
     * yyyy-mm-ddThh-mm-ssZ, this string is further used to find the correct node in the
     * locationforecast-xml
     */
    public WeatherXMLParser(){
            xpath = XPathFactory.newInstance().newXPath();

            //Get current date
        Date date = new Date();
        dato = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(date);
            dato += "T";

            //Get current time and round down to last whole hour
        SimpleDateFormat tid = new SimpleDateFormat("HH-mm-ss", Locale.getDefault());
            tid.setTimeZone(TimeZone.getTimeZone("GMT+1"));
        String time = tid.format(date);
            time = time.substring(0,2);
            dato += time;
            dato += ":00:00Z";
            Log.d("DATO",dato);
    }

    /**
     * Parses temperature at users current location from API.
     * Xpath is used to find the correct node, xpath is compiled and evaluated to get the value
     * of the temperature node
     * @param in The XML to parse
     */
    public void parseTemperature(Document in){
            String temp = "";
            //Construct xpath string for fetching temperature from locationforecastobject
        String locationForecastString = "/weatherdata/product/time[@from='";
            locationForecastString += dato;
            locationForecastString += "']/location[1]/temperature/@value";
            Log.d("XPATHexpr", locationForecastString);
        XPathExpression expression;
        try {
                expression = xpath.compile(locationForecastString);
                temperature = expression.evaluate(in);
            } catch(XPathExpressionException e){
                Log.d("WeatherXMLParser", e.toString());
            }
            //Constructs xpath string for fetching symbolid from locationforecastobject
        String weatherIconString = "/weatherdata/product/time[@from='";
            weatherIconString += dato;
            weatherIconString += "'][2]/location/symbol/@number";
            try {
                expression = xpath.compile(weatherIconString);
                symbolId = expression.evaluate(in);
                Log.d("SYMBOL", weatherIconString);
            } catch(XPathExpressionException e) {
                Log.d("WeatherXMLParser", e.toString());
            }


    }

    /**
     * Establishes a connection to the weathericon-url and downloads the returned image further
     * saving it in a bitmap
     * @param apiLink API link address
     */
    public void parseImage(String apiLink){
        URL url;
        try {
            url = new URL(apiLink);
            URLConnection conn = url.openConnection();
            image = BitmapFactory.decodeStream(conn.getInputStream());
        } catch(IOException e) {
            Log.d("WeatherXMLParser", e.toString());
        }
    }

    /**
     * @return the weathericon as a bitmap
     */
    public Bitmap getImage() {
        return image;
    }

    /**
     * @return Return the temperature as a String
     */
    public String getTemperature() {
        return temperature;
    }

    /**
     * @return symbolid for the current weather condition
     */
    public String getSymbolId() {
        return symbolId;
    }
}
