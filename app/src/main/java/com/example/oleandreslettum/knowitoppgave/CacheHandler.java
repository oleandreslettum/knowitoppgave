package com.example.oleandreslettum.knowitoppgave;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.preference.PreferenceManager;
import android.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

/**
 * This class handles the caching mechanisms for the application.
 * It has functions to write and read image from file, and temperature/timestamp to shared preferences
 *
 * Used to reduce network traffic.
 *
 * The activity that want to cache data creates a new CacheHandler object and uses the
 * cacheImageToFile and cacheTimeAndTempToSP to help in caching data
 *
 * The activity that want to read cached data creates a new CacheHandler object, uses the
 * checkIfCanUseCache-function to check if cache can be used(less than five minutes since last write,
 * and less than 500m distance between the coordinates at the two moments in time). If cache can
 * be used, functions to get cached data are called
 *
 * Created by oleandreslettum on 08.01.2017.
 */

class CacheHandler {
    private Bitmap imageIcon;
    //Cache is only reusable within 5 minutes(300000 milliseconds)
    private final static Long cacheTime = 300000L;
    Context applicationContext = null;
    final File imageFile;
    private String temperature;


    /**
     * Empty constructor, used to construct an object which will be used to check if cache files can
     * be used
     */
    CacheHandler(Context context) {
        imageFile = new File(context.getCacheDir(), "weathericon.png");
        applicationContext = WeatherActivity.getAppContext();
    }

    /**
     * Saves timestamp
     */
    public void cacheTimeAndTempToSP(String temperature, String oldLatitude, String oldLongitude) {
        Date date = new Date();
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(applicationContext);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong("timestamp", date.getTime());
        editor.putString("temp", temperature);
        editor.putString("oldLatitude", oldLatitude);
        editor.putString("oldLongitude", oldLongitude);
        editor.commit();
    }
    /**
     * Reads bitmap from icon.png-file
     */
    public Bitmap getBitmapFromFile() {
        return BitmapFactory.decodeFile(imageFile.getPath());
    }
    /**
     * Checks if the cache file are less than 5 minutes old, also reads temperature
     * @return if can use the last written cache file
     */
    public boolean checkIfCanUseCache(Double newLatitude, Double newLongitude) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(applicationContext);
        Long timeStamp = sharedPref.getLong("timestamp", 0L);

        return(isWithin5Minutes(timeStamp) && isWithin500Meters(newLatitude, newLongitude));

    }

    /**
     * Checks to see if new and old location are within 500 meters between each others
     * @param newLatitude The new latitude user has arrived to
     * @param newLongitude The new longitude user has arrived to
     * @return returns if two locations are within 500 meters to each other or not
     */
    public boolean isWithin500Meters(Double newLatitude, Double newLongitude) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(applicationContext);
        Double oldLatitude = Double.parseDouble(sharedPref.getString("oldLatitude", null));
        Double oldLongitude = Double.parseDouble(sharedPref.getString("oldLongitude", null));

        Location oldLocation = new Location("");
        Location newLocation = new Location("");

        return (oldLocation.distanceTo(newLocation) <= 500F);

    }

    /**
     * Checks to see if two times(defined as milliseconds since 1.1.1970) are within 5 minutes of
     * each other
     * @param timeStamp Timestamp when cache was written
     * @return returns if two times are within 5 minutes of each other
     */
    public boolean isWithin5Minutes(Long timeStamp) {
        Date date = new Date();
        Long currentTime = date.getTime();

        return timeStamp.longValue() > 0L && ((currentTime - timeStamp) <= cacheTime);
    }
    public String getTemperature() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(applicationContext);
        return sharedPref.getString("temp", null);
    }

    /**
     * Caches a png-image to file
     */
    public void cacheImageToFile(Bitmap image) {
        FileOutputStream fOut;
        try {
            fOut = new FileOutputStream(imageFile, false);
            image.compress(Bitmap.CompressFormat.PNG, 85, fOut);
            fOut.flush();
            fOut.close();
        } catch(IOException e) {
            Log.d("cacheFile", e.toString());
        }

    }
}
