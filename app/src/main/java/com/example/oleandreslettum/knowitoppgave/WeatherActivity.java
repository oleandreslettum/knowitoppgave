package com.example.oleandreslettum.knowitoppgave;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Main activity for the application, fetches weather data from Meterologisk institutt api.met.no
 * and shows weather icon and temperature at users current location when opening application or
 * application regains focus. The application first checks if there are cached data that can be used,
 * this is done to minimize network traffic.
 *
 * If there isn't any data, an asynctask is started which fetches data from the API and further
 * updates the UI and caches the newly fetched data
 *
 * Created by Ole Andre Slettum
 */
public class WeatherActivity extends AppCompatActivity {
    private Context _this = this;
    private String latitude;
    private String longitude;
    private String temperature = null;
    private Bitmap image = null;
    private String symbolId = null;
    private final int REQUEST_LOCATION_PERMISSION = 1;
    public static Context contextOfApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
    }

    /**
     * Updates UI with cachedata or new from API, checks first if cache can be used(less than 5 minutes old
     * and less than 500 meters difference in distance ),
     * else it starts the chain of functions that tries to parse data from api
     */
    @Override
    protected void onResume() {
        super.onResume();
        findViewById(R.id.button).setVisibility(View.INVISIBLE);
        contextOfApp = getApplicationContext();
        getGpsPermission(findViewById(android.R.id.content));

    }
    /**
     * Checks that app has the required location access permission
     * If app already has required permissions, longitude and latitude is fetched from GPS
     * and an asynchronous task that fetches data from API is started
     *
     * If app doesn't have required permissions, user are asked to grant permission and callback is fired
     */
    public void getGpsPermission(View view) {
            boolean retrieve = false;

            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                            new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION_PERMISSION);
                return;
            } else {
                if(hasNetworkConnection()) {
                    //Fetch latitude and longitude
                    getLocationInfo();
                    //Starts an async task that fetches weatherdata from api.met.no
                    new DownloadWeatherDataTask().execute("");
                } else {
                    Toast.makeText(this, R.string.no_network,
                            Toast.LENGTH_LONG).show();
                }
            }
    }

    /**
     * Callback function fired after asking GPS-access permission
     * If permission granted, start fetching data from weather api
     * If permission refused, show short toast that the app doesn't have required permissions
     *
     * @param requestCode  requestCode The request code passed in
     * @param permissions  permissions array of permissions needed
     * @param grantResults result of asked permissions
     */
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(hasLocationConnection()) {
                        //Fetch latitude and longitude
                        getLocationInfo();
                        //Starts an async task that fetches weatherdata from api.met.no
                        new DownloadWeatherDataTask().execute("");
                    } else{
                        Toast.makeText(this,R.string.no_network,
                                Toast.LENGTH_LONG).show();
                    }

                } else {
                    Button button = (Button) findViewById(R.id.button);
                    button.setVisibility(View.VISIBLE);

                    button.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            getGpsPermission(findViewById(android.R.id.content));
                        }
                    });
                    //In case the user denies location-permission
                    Toast.makeText(this, R.string.permission_denied, Toast.LENGTH_LONG).show();
                }
        }
    }

    /**
     * Creates a new instance of the GPSTracker-class and uses it to get current longitude and latitude
     */
    private void getLocationInfo(){
        try {
            GPSTracker tracker = new GPSTracker(this);
            latitude = Double.toString(tracker.getLatitude());
            longitude = Double.toString(tracker.getLongitude());
        } catch(SecurityException e) {
            Log.d("WeatherActivity", e.toString());
        }
    }

    /**
     * Checks to see if phone has gps or network connection, used to determine if app
     * can try to get location information "safely"
     * @return has gps/network connection or not
     */
    private boolean hasLocationConnection() {
        LocationManager locationManager = (LocationManager) this
                .getSystemService(LOCATION_SERVICE);

        return(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
                || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER));
    }

    /**
     * Checks to see if phone has network connection, used to determine if app should try to
     * fetch data from the weather API
     * @return if phone has network connection
     */
    public boolean hasNetworkConnection() {
        LocationManager locationManager = (LocationManager) this
                .getSystemService(LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    /**
     * Updates UI if both temperature and image are not null, meaning app successfully fetched data
     * from API or from cache(file and SP).
     *
     * If no values have been fetched, button with "Get weather"
     * are made visible which makes user able to try again without restarting application.
     * TextView are also set to No network connection: Try again:, informing the user of the problem"
     */
    private void updateUI() {
        TextView degrees = (TextView) findViewById(R.id.degrees);
        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        if(temperature!=null && image!=null) {
            //Make sure the "Get weather" button is not in the view
            Button button = (Button) findViewById(R.id.button);
            button.setVisibility(View.INVISIBLE);

            degrees.setVisibility(View.VISIBLE);
            temperature += "\u2103";
            degrees.setText(temperature);

            imageView.setVisibility(View.VISIBLE);
            image = getResizeBitmap(image, 150, 150);
            imageView.setImageBitmap(image);

        } else {
            imageView.setVisibility(View.INVISIBLE);

            Button button = (Button) findViewById(R.id.button);
            button.setVisibility(View.VISIBLE);

            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    getGpsPermission(findViewById(android.R.id.content));
                }
            });
            degrees.setText("No internet connection\n           Try again:");
        }
    }

    /**
     * Used to get application context in non-activity classes
     * @return application context
     */
    public static Context getAppContext() {
        return contextOfApp;
    }


    /**
     * Resizes a bitmap
     * Directly copied from third answer in post:
     * http://stackoverflow.com/questions/9223131/how-to-create-a-bigger-bitmap-from-an-existing-bitmap
     * @param bitmap bitmap to resize
     * @param newHeight the new height to scale up/down to
     * @param newWidth the new width to scale up/down to
     * @return bitmap resized
     */
    public Bitmap getResizeBitmap(Bitmap bitmap, int newHeight, int newWidth)
    {

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        float scaleWidth = ((float)newWidth)/width;
        float scaleHeight = ((float)newHeight)/height;

        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);

        return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);

    }

    /**
     * Gets the locationforecast data from API and returns the xml as a Document-object
     * @return the locationforecast xml as a Document object
     */
    private Document establishTemperatureAPIConnection() {
        Document in  = null;
        URL url = null;
        try {
            //Build the API-link
            String urlString = makeAPILink();

            url = new URL(urlString);
            URLConnection conn = url.openConnection();

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setNamespaceAware(true);

            //Create a Documentobject from the inputstream
            in = factory.newDocumentBuilder().parse(conn.getInputStream());
        } catch (IOException | ParserConfigurationException | SAXException e) {
            Log.d("WeatherActivity", e.toString());
        }
        return in;
    }

    /**
     * Builds the API link by appending latitude and longitude-data to API-link
     * @return returns the result as a string
     */
    private String makeAPILink() {
        String startURL = "http://api.met.no/weatherapi/locationforecast/1.9/?lat=";

        startURL += latitude;
        startURL += ";lon=";
        startURL += longitude;
        Log.d("WeatherActivity", startURL);
        return startURL;
    }

    /**
     * Uses WeatherXMLParser to parse temperature and weather icon number from XML
     * @param in The document which contain the XML to be parsed
     */
    private void parseTemperatureFromXML(Document in) {
        WeatherXMLParser parser = new WeatherXMLParser();
        parser.parseTemperature(in);
        //Get the parsed data
        temperature = parser.getTemperature();
        symbolId = parser.getSymbolId();

    }

    /**
     * Constructs api-link to the weather icon for current weather condition, creates an
     * instance of WeatherXMLParser which is used to parse the icon as a bitmap
     */
    private void parseImageFromXML() {
        String imageType = "content_type=image/png";
        String iconLink = "http://api.met.no/weatherapi/weathericon/1.1/?symbol=";

        iconLink += symbolId;
        iconLink += ";";
        iconLink += imageType;
        Log.d("WeatherActivity", iconLink);
        WeatherXMLParser parser = new WeatherXMLParser();
        parser.parseImage(iconLink);
        image = parser.getImage();
    }


    /**
     * Asynchronous task that establishes connection to api.met.no, uses WeatherXMLParser to parse data
     * from the API and ultimately updates user UI with weather data
     */
    private class DownloadWeatherDataTask extends AsyncTask<String, Integer, Long> {

        /**
         * Establishes connection to the API and calls function that parses temperature from the xml
         * @param urls Parameters sent to task
         * @return Long
         */
        protected Long doInBackground(String... urls) {
            //Get the locationforecast-xml document from API
            Document in = establishTemperatureAPIConnection();
            //Parse temperature and image from the document
            if(hasNetworkConnection()) {
                parseTemperatureFromXML(in);
                parseImageFromXML();
            }
            return 1L;
        }

        /**
         * Updates UI with weathericon and temperature at users current location
         * @param aLong
         */
        @Override
        protected void onPostExecute(Long aLong) {
            super.onPostExecute(aLong);

            CacheHandler handler = new CacheHandler(_this);
            if(image!=null && temperature !=null) {
                handler.cacheImageToFile(image);
                handler.cacheTimeAndTempToSP(temperature,latitude,longitude);
            }

            //Update UI if successfully fetched values from weather API
            updateUI();
        }
    }
}
